package albuquerque.training.cassandra.indexes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

/**
 * This example will cover the main differences between partition keys, clustering keys, and compound keys.
 * <p/>
 * Note that you will need a working Cassandra cluster and to replace the port and contact point below.
 */
public class KeysExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeysExample.class);

    private static final String CONTACT_POINT = "coreos01";
    private static final int PORT = 9042;

    public static void main(String[] args) {
        String clusterName = "My cluster";

        Cluster cluster = Cluster.builder()
                .withClusterName(clusterName)
                .addContactPoint(CONTACT_POINT)
                .withPort(PORT)
                .build();

        // Get a valid session to allow our cluster to be queried.
        Session session = cluster.connect();

        // Start from scratch
        session.execute("DROP TABLE IF EXISTS mykeyspace.mytable_1;");
        session.execute("DROP TABLE IF EXISTS mykeyspace.mytable_2;");
        session.execute("DROP TABLE IF EXISTS mykeyspace.mytable_3;");
        session.execute("DROP KEYSPACE IF EXISTS mykeyspace;");

        // Create our keyspace
        session.execute("CREATE KEYSPACE mykeyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'};");

        // Create our first table with a primary key. The primary key (or partition key) is responsible for distributing data
        // across the cluster nodes
        session.execute("CREATE TABLE mykeyspace.mytable_1 (\n"
                + "    id bigint,\n"
                + "    language text,\n"
                + "    content text,\n"
                + "    PRIMARY KEY (id)\n"
                + ")");

        // A second table with a key. The first part of the key is the partition key, the second is the clustering key.
        // The partition key (id) will determine how data will be distributed across the cluster nodes.
        // The clustering key (language) will be used to sort data within a partition.
        session.execute("CREATE TABLE mykeyspace.mytable_2 (\n"
                + "    id bigint,\n"
                + "    language text,\n"
                + "    content text,\n"
                + "    PRIMARY KEY (id, language)\n"
                + ")");

        // A third table with multiple columns for partition and clustering keys.
        // The partition keys (id and language) will determine how data will be distributed across the cluster nodes.
        // The clustering key (country) will be used to sort data within a partition.
        session.execute("CREATE TABLE mykeyspace.mytable_3 (\n"
                + "    id bigint,\n"
                + "    language text,\n"
                + "    country text,\n"
                + "    content text,\n"
                + "    PRIMARY KEY ((id, language), country)\n"
                + ")");

        // Add a few records to table 1
        session.execute("INSERT INTO mykeyspace.mytable_1 (id, language, content) VALUES (1, 'en', 'foo');");
        session.execute("INSERT INTO mykeyspace.mytable_1 (id, language, content) VALUES (2, 'pt', 'bar');");

        // To table 2
        session.execute("INSERT INTO mykeyspace.mytable_2 (id, language, content) VALUES (1, 'en', 'foo');");
        session.execute("INSERT INTO mykeyspace.mytable_2 (id, language, content) VALUES (2, 'pt', 'bar');");

        // And to table 3
        session.execute("INSERT INTO mykeyspace.mytable_3 (id, language, country, content) VALUES (1, 'en', 'US', 'foo');");
        session.execute("INSERT INTO mykeyspace.mytable_3 (id, language, country, content) VALUES (2, 'pt', 'PT', 'bar');");

        // Note that our keys impact our ability to query data
        // On our first table, we are able to query data by id, our partition key, but not by any other field
        ResultSet resultSet1stTable = session.execute("SELECT id, content FROM mykeyspace.mytable_1 WHERE id = ?", 1L);
        LOGGER.info("row={}", resultSet1stTable.one());

        // Where as on the second table we can query by id and by language
        // We can not however query just by language, as the language is a clustering key and not a primary key
        ResultSet resultSet2ndTable = session.execute("SELECT id, content FROM mykeyspace.mytable_2 WHERE id = ? and language = ?", 1L, "en");
        LOGGER.info("row={}", resultSet2ndTable.one());

        // The third table is very similar to the 2nd one, with some minor differences though.
        // We need to restrict our query by id and language, as they are both part of our partition key
        // We can optionally, as with the second table, restrict with the country as well, our clustering key
        ResultSet resultSet3rdTable = session.execute("SELECT id, content FROM mykeyspace.mytable_3 WHERE id = ? and language = ? and country = ?", 1L, "en", "US");
        LOGGER.info("row={}", resultSet3rdTable.one());

        // ...and we delete up everything at the end
        session.execute("DROP TABLE mykeyspace.mytable_1;");
        session.execute("DROP TABLE mykeyspace.mytable_2;");
        session.execute("DROP TABLE mykeyspace.mytable_3;");
        session.execute("DROP KEYSPACE mykeyspace;");

        session.close();
        cluster.close();
    }
}

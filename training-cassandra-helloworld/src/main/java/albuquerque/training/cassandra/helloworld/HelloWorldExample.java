package albuquerque.training.cassandra.helloworld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * On this example we connect to an existing cluster, create a simple table, insert a few of records, retrieve them and clean up everything at the end.
 * <p/>
 * Note that you will need a working Cassandra cluster and to replace the port and contact point below.
 */
public class HelloWorldExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldExample.class);

    private static final String CONTACT_POINT = "coreos01";
    private static final int PORT = 9042;

    public static void main(String[] args) {
        String clusterName = "My cluster";

        Cluster cluster = Cluster.builder()
                .withClusterName(clusterName)
                .addContactPoint(CONTACT_POINT)
                .withPort(PORT)
                .build();

        // Get a valid session to allow our cluster to be queried.
        Session session = cluster.connect();

        // Start from scratch
        session.execute("DROP TABLE IF EXISTS mykeyspace.mytable;");
        session.execute("DROP KEYSPACE IF EXISTS mykeyspace;");

        // Create our keyspace
        session.execute("CREATE KEYSPACE MY_KEYSPACE WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'};");

        // Create our column family (table)
        session.execute("CREATE TABLE MY_KEYSPACE.MY_TABLE (\n"
                + "    id bigint,\n"
                + "    content text,\n"
                + "    PRIMARY KEY (id)\n"
                + ")");

        // Add a few records
        session.execute("INSERT INTO MY_KEYSPACE.MY_TABLE (id, content) VALUES (1, 'foo');");
        session.execute("INSERT INTO MY_KEYSPACE.MY_TABLE (id, content) VALUES (2, 'bar');");

        // And retrieve them
        ResultSet result = session.execute("SELECT * FROM MY_KEYSPACE.MY_TABLE");
        for (Row row : result) {
            long id = row.getLong("id");
            String content = row.getString("content");
            LOGGER.info("id={}, content={}", id, content);
        }

        // ...and we delete up everything at the end
        session.execute("DROP TABLE MY_KEYSPACE.MY_TABLE;");
        session.execute("DROP KEYSPACE MY_KEYSPACE;");

        session.close();
        cluster.close();
    }
}

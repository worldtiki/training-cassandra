package albuquerque.training.cassandra.simplestatements;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

/**
 * Use PreparedStatement for queries that are executed multiple times in your application.
 * When you prepare the statement, Cassandra will parse the query string, cache the result and return a unique id to that statement.
 * When you bind and execute a prepared statement, the driver will only send the identifier, which allows Cassandra to skip the parsing phase.
 * <p/>
 * Note that you will need a working Cassandra cluster and to replace the port and contact point below.
 */
public class PreparedStatementsExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreparedStatementsExample.class);

    private static final String CONTACT_POINT = "coreos01";
    private static final int PORT = 9042;

    public static void main(String[] args) {
        String clusterName = "My cluster";

        Cluster cluster = Cluster.builder()
                .withClusterName(clusterName)
                .addContactPoint(CONTACT_POINT)
                .withPort(PORT)
                .build();

        // Get a valid session to allow our cluster to be queried.
        Session session = cluster.connect();

        // Start from scratch
        session.execute("DROP TABLE IF EXISTS mykeyspace.mytable;");
        session.execute("DROP KEYSPACE IF EXISTS mykeyspace;");

        // Create our keyspace
        session.execute("CREATE KEYSPACE mykeyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'};");

        // Create our column family (table)
        session.execute("CREATE TABLE mykeyspace.mytable (\n"
                + "    id bigint,\n"
                + "    content text,\n"
                + "    PRIMARY KEY (id)\n"
                + ")");

        // Add a few records
        session.execute("INSERT INTO mykeyspace.mytable (id, content) VALUES (1, 'foo');");
        session.execute("INSERT INTO mykeyspace.mytable (id, content) VALUES (2, 'bar');");
        session.execute("INSERT INTO mykeyspace.mytable (id, content) VALUES (3, 'foobar');");

        // We now prepare our select statement
        PreparedStatement preparedStatement = session.prepare("SELECT id, content FROM mykeyspace.mytable where id = ?");

        // Bind the variables and execute it
        ResultSet result = session.execute(preparedStatement.bind(1L));
        LOGGER.info("row={}", result.one());

        // Bind with a different value and execute it again
        ResultSet resultForAnotherBoundStatement = session.execute(preparedStatement.bind(2L));
        LOGGER.info("row={}", resultForAnotherBoundStatement.one());

        // ...and we delete up everything at the end
        session.execute("DROP TABLE mykeyspace.mytable;");
        session.execute("DROP KEYSPACE mykeyspace;");

        session.close();
        cluster.close();
    }
}

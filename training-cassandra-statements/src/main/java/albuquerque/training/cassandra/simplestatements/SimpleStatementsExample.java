package albuquerque.training.cassandra.simplestatements;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;

/**
 * SimpleStatement is used for queries that will be executed only once (or a few times) in your application.
 * If you execute the same query often consider using a prepared statement instead.
 * <p/>
 * Note that you will need a working Cassandra cluster and to replace the port and contact point below.
 */
public class SimpleStatementsExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleStatementsExample.class);

    private static final String CONTACT_POINT = "coreos01";
    private static final int PORT = 9042;

    public static void main(String[] args) {
        String clusterName = "My cluster";

        Cluster cluster = Cluster.builder()
                .withClusterName(clusterName)
                .addContactPoint(CONTACT_POINT)
                .withPort(PORT)
                .build();

        // Get a valid session to allow our cluster to be queried.
        Session session = cluster.connect();

        // Start from scratch
        session.execute("DROP TABLE IF EXISTS mykeyspace.mytable;");
        session.execute("DROP KEYSPACE IF EXISTS mykeyspace;");

        // Create our keyspace
        session.execute("CREATE KEYSPACE mykeyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'};");

        // Create our column family (table)
        session.execute("CREATE TABLE mykeyspace.mytable (\n"
                + "    id bigint,\n"
                + "    content text,\n"
                + "    PRIMARY KEY (id)\n"
                + ")");

        // Add a few records
        session.execute("INSERT INTO mykeyspace.mytable (id, content) VALUES (1, 'foo');");
        session.execute("INSERT INTO mykeyspace.mytable (id, content) VALUES (2, 'bar');");
        session.execute("INSERT INTO mykeyspace.mytable (id, content) VALUES (3, 'foobar');");

        // We can now retrieve one of the records using a Simple Statement
        SimpleStatement simpleStatement = new SimpleStatement("SELECT id, content FROM mykeyspace.mytable WHERE id = ?", 1L);
        ResultSet resultForSimpleStatement = session.execute(simpleStatement);
        for (Row row : resultForSimpleStatement) {
            LOGGER.info("row={}", row);
        }

        // Which can also have this convenient format
        ResultSet resultForShortcutForm = session.execute("SELECT id, content FROM mykeyspace.mytable WHERE id = ?", 2L);
        for (Row row : resultForShortcutForm) {
            LOGGER.info("row={}", row);
        }

        // ...and we delete up everything at the end
        session.execute("DROP TABLE mykeyspace.mytable;");
        session.execute("DROP KEYSPACE mykeyspace;");

        session.close();
        cluster.close();
    }
}

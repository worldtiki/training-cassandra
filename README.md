# Getting Started

This project may be used as a repository of examples of how to use Cassandra.
It contains several maven submodules, each with a working example of a specific feature or pattern.

## What is Cassandra

Cassandra is a fast distributed database built for high availability and linear scalability.
It has no single point of failure and runs on commodity hardware.
It does not, however, replace a traditional relational database.

### Startup and node failure detection ###

Cassandra uses the gossip protocol, which consists of exchanging state information about other nodes, allowing the cluster to detect new nodes and node failures.
It also uses seed nodes at startup which are "well known nodes" that are only used by new nodes to obtain information about the remaining members of the cluster.

### The hash ring ###

A Cassandra cluster is formed of several nodes. There are not master or slave nodes in Cassandra and each and every node should be able to process any given request.
A cluster is visualized as a ring because of the way data is distributed.
Data is distributed using a hash algorithm on the item's key, and each cluster node is in turn responsible for managing a given token range.
 
### Replication ###

Data replication is configured at keyspace creation. A keyspace is similar to a database or schema, and consists of a set of tables.
A replication factor (RF) means the number of replicas that will exist in our cluster. For example, with a RF of 3, we will have our data on 3 nodes, the first replica and two subsequent replicas. 

### Consistency levels ###

With Cassandra you can define the consistency level (CL) that you want on a per query basis.
The most common levels are:

|--------|-------------------------------------------------------|
| ALL    | Waits to hear back from every node                    |
| QUORUM | Waits to hear back from a majority of nodes (n/2 + 1) |
| ONE    | Waits to hear back from just one node                 |

* n = our replication factor

But what exactly does this mean? Continue reading to find out how these levels are used in the read and write path.

### Write path ###

Clients can connect to any node to insert data to the cluster (most recent drivers are aware of the partitioning algorithm and are able to direct nodes to the node that contains the data for that specific key).
The node that is handling a request is called the coordinator node.
Depending on the replication factor for that keyspace, the coordinator node will forward the change to a given number of nodes, for example, with a RF=3, the coordinator node would forward our change to the first replica and to two other nodes.
It will then wait from the nodes to acknowledge the change, depending on the consistency level. For example, with a RF=3, if we set a CL=QUOROM, it will wait for 2 nodes to acknowledge the mutation.  

### Read pattern ###

Same as with writes, clients can connect to any node to read data from the cluster.
The node they connect to is the coordinator node and is responsible for returning the requested data.
Same as with the write operation, the replication factor and the consistency level determines the number of nodes that need to respond before the coordinator can return with the read response.
For example, with a RF=3 and CL=QUORUM, the coordinator will wait for replies from two nodes.

### Deletes ###

Cassandra deletes data differently from traditional relational databases.
When data is deleted, either explicitly with a delete operation or when the ttl expires, that data is market with a tombstone and eventually removed during the compaction process, allowing for very fast deletes.
Another reason is to resolve possible conflicts. Cassandra uses the timestamp of each column to determine the most recent value. As with any other operation, a delete operation must also have an associated timestamp so that Cassandra knows if it happened before or after another update and what the current value of that column is.
As such, instead of deleting data, Cassandra creates a tombstone which has an associated timestamp.

## Examples / Patterns

This project covers most of the basic features and patterns of using Cassandra.

| Module       | Description                                                                                             |
| -------------|---------------------------------------------------------------------------------------------------------|
| Hello World  | End to end example, from the creation of a keyspace/table to adding and retrieving data from Cassandra. |
| Consistency  | Examples of how to work with different consistency levels                                               |
| Keys         | Differences between primary keys, partition keys, clustering keys, compound keys, ...                   |
| Statements   | Simple statements vs prepared statements                                                                |


## References

- [Cassandra Java driver](https://github.com/datastax/java-driver/)